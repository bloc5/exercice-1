package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

/**
 * Cette classe d'énumération liste les extensions de fichiers à récuperer dans les métadata du badge afin
 * de faire correspondre ce type dans le parsage lors de la récupération de l'image.
 */

public enum ManagedImages {
   jpeg("image/jpeg"), jpg("image/jpeg"), png("image/png"), gif("image/gif");
   private String mimeType;
   ManagedImages(String mimeType) {
     this.mimeType = mimeType;
   }
   public String getMimeType() {
     return mimeType;
   }
 }
